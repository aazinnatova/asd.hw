using System;
using System.Linq;

namespace LinkedListPractice
{


    public class WordSet
    {
        private class Node<String>
        {
            public string value;
            public Node<String> next;

            public Node(string value)
            {
                this.value = value;
            }
        }

        private Node<String> Head;
        private Node<String> Tail;
        private int size;

        public void insert(string item)
        {
            var node = new Node<String>(item);
            if (IsEmpty())
            {
                Head = Tail = node;
                return;
            }
            var current = Head;
            if (String.Compare(current.value, item) < 0)
            {
                node.next = Head;
                Head = node;
                size++;
                return;
            }

            while (current != null)
            {

                if (String.Compare(current.next.value, item) < 0)
                {
                    var newNode = new Node<String>(item);
                    newNode.next = current.next;
                    current.next = node;
                    node.next = newNode.next;
                    return;
                }
                current = current.next;
            }
            Tail.next = node;
            Tail = node;
            size++;
        }

        public WordSet(string[] arr)
        {
            string[] copyArr = arr;
            Array.Sort(copyArr, StringComparer.InvariantCulture);
            foreach (string elem in copyArr)
            {
                insert(elem);
            }
        }

        public void delete(string Value)
        {
            if (Head == null) return;

            if (Head.value == Value)
            {
                Head = Head.next;
                return;
            }

            var n = Head;
            while (n.next != null)
            {
                if (n.next.value == Value)
                {
                    n.next = n.next.next;
                    return;
                }

                n = n.next;
            }
        }
        public void removePalindrome()
        {
            var n = Head;
            while (n.next != null)
            {
                char[] cArray = n.value.ToCharArray();
                Array.Reverse(cArray);
                string nvcopy = new string(cArray);
                if (n.value.SequenceEqual(nvcopy))
                {
                    delete(n.value);
                }
                n = n.next;
            }
        }

        public void AddFirst(string item)
        {
            var node = new Node<String>(item);

            if (IsEmpty())
                Head = Tail = node;
            else
            {
                node.next = Head;
                Head = node;
            }

            size++;
        }

        private bool IsEmpty()
        {
            return Head == null;
        }

        public int IndexOf(string item)
        {
            int index = 0;
            var current = Head;
            while (current != null)
            {
                if (current.value == item) return index;
                current = current.next;
                index++;
            }
            return -1;
        }

        public bool Contains(string item)
        {
            return IndexOf(item) != -1;
        }

        public void RemoveFirst()
        {
            if (IsEmpty())
                throw new InvalidOperationException();

            if (Head == Tail)
                Head = Tail = null;
            else
            {
                var second = Head.next;
                Head.next = null;
                Head = second;
            }

            size--;
        }

        public void RemoveLast()
        {
            if (IsEmpty())
                throw new InvalidOperationException();
                if (Head == Tail)
                Head = Tail = null;
            else
            {
                var previous = GetPrevious(Tail);
                Tail = previous;
                Tail.next = null;
            }

            size--;
        }

        private Node<String> GetPrevious(Node<String> node)
        {
            var current = Head;
            while (current != null)
            {
                if (current.next == node) return current;
                current = current.next;
            }
            return null;
        }

        public int Size()
        {
            return size;
        }
    }
}